// Transform the Load More button
transformBtn = () => {
    loadMoreBtn.className = "button is-medium is-fullwidth is-static";
    loadMoreBtn.innerHTML = "No more results";
}

// Calculate and show total number of results
var items = document.querySelectorAll("div.search-result");
var text = "Total results: " + items.length;
document.getElementById('results').innerHTML = text;


// Show only the first 10 results
var loadMoreBtn = document.querySelector("#loadMore");
var maxItems = 10; // TO-DO: Read variable from JSON
var hiddenClass = "is-hidden";

if (items.length <= maxItems) {
    transformBtn();
};

[].forEach.call(items, function(item, idx){
    if (idx > maxItems - 1) {
        item.classList.add(hiddenClass);
    }
});


// Show next 10 results
loadMoreBtn.addEventListener('click', () => {

    [].forEach.call(document.querySelectorAll('.' + hiddenClass), function(item, idx){
        if (idx < maxItems - 1) {
            item.classList.remove(hiddenClass);
        }
        if (idx === 0) {
            item.scrollIntoView({
                behavior: 'smooth'
            });
        }
        if ( document.querySelectorAll('.' + hiddenClass).length === 0) {
            transformBtn();
        }

    });

});