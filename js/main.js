// Display an hamburger menu on mobile devices
document.addEventListener('DOMContentLoaded', () => {

    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach( el => {
            el.addEventListener('click', () => {

                // Get the target from the "data-target" attribute
                const target = el.dataset.target;
                const $target = document.getElementById(target);

                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

});

// Highlights the current page in the menu
document.addEventListener('DOMContentLoaded', () => {

    var pages = document.getElementById("leftMenu").querySelectorAll('a');
    var active = 0;

    for (var i = 0; i < pages.length; i++) {
        pages[i].classList.remove('is-active');
        if (pages[i].href === document.URL) {
            active = i;
        };
    };

    pages[active].classList.add('is-active');

    if (pages[active].href != document.URL) {
        pages[active].classList.remove('is-active');
    };

});