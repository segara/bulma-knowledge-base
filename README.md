# Bulma Knowledge Base for Pico

In Segara we wanted to build a knowledge base on top of a Nextcloud instance, using [Pico CMS for Nextcloud](https://github.com/nextcloud/cms_pico). The available themes did not fully meet our needs, so we ended up crafting this **bulma-based knowledge base theme for Pico**, which may be useful for individuals and small to mid sized teams looking for a site where sharing different pieces of knowledge (snippets, technical docs, procedures, guides...). It is not a replacement for dedicated technologies like [wiki](https://en.wikipedia.org/wiki/Wiki_software) or [EDRMS](https://en.wikipedia.org/wiki/Electronic_document_and_records_management_system) software.

## Features

- Modern and fully-responsive design thanks to [Bulma CSS](https://bulma.io/).
- Fulltext search.
- Results page that shows the total number of results and paginates the results.
- Sidebar that shows pages and nested pages.
- Pages that support Table of Contents and code syntax highlighting.
- Navbar with logo and hamburger menu.
- 100% free software! The source code is licensed MIT.

## Screenshots

![Index screenshot](screenshots/screenshot-index.png)

![Code screenshot](screenshots/screenshot-code.png)

![Results page screenshot](screenshots/screenshot-results.png)

<img src="screenshots/screenshot-mobile.png" width="400">

<img src="screenshots/screenshot-mobile-menu.png" width="400">

## Installation

1. This theme requires [Pico Pages List](https://github.com/nliautaud/pico-pages-list) and [Pico Search](https://github.com/PontusHorn/Pico-Search) plugins, so make sure to install them following the instructions provided in the links.
2. Download/Clone this repository.
3. Copy/Upload the folder `bulma-knowledge-base` into the `themes` directory of your Pico installation.

By default, the navbar will display the site title. If you want to display a logo:

1. Copy/Upload the logo into the `themes/bulma-knowledge-base/img` directory of your Pico installation.
2. Include the link to your logo in the `_meta.md` file:

```
---
Logo: %theme_url%/img/your-awesome-logo.png
---
```

## Usage

### Table of contents

The ToC is not automatically rendered based on the page headers. To use it, you need to declare the structure in the markdown file metadata. For example:

```
---
Title: Welcome
Description: Pico is a stupidly simple, blazing fast, flat file CMS.

toc:
    welcome: Welcome to Pico
    creating-content:
        _title: Creating Content
        text-file-markup: Text File Markup
        blogging: Blogging
    customization:
        _title: Customization
        themes: Themes
        plugins: Plugings
    config: Config
    documentation: Documentation
---
```

In addition, if you want to have links to the sections of the document in the ToC, you need to identify each header with the name given to it in the ToC, appending this code to the header: `{#<section-name>}`. For example:

```
## Welcome to Pico {#welcome}

![Nextcloud Logo](%assets_url%/image.png)

Congratulations, you have successfully installed [Pico CMS for Nextcloud][App],
utilizing [Pico][] %version%. %meta.description% <!-- replaced by the above Description header -->

## Creating Content {#creating-content}

Pico is a flat file CMS. This means there is no administration backend or
database to deal with. You simply create `.md` files in the `content` folder
and those files become your pages. For example, this file is called `index.md`
and is shown as the main landing page.
```

### Syntax highlighting

This theme use [Prism.js](https://prismjs.com/) for syntax highlighting.

To apply it, just write the name of the language after the three back-ticks that open the block of code. For example:

````
```python
print("Hello world")
```
````
